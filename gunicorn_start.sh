#!/bin/sh
NAME="rentyapp"
DJANGODIR="/var/www/html/rentycl/rentyapp"
NUM_WORKERS=3
echo "Starting Renty CL Django Application"
cd $DJANGODIR
exec gunicorn -w $NUM_WORKERS $NAME.wsgi:application --bind 127.0.0.1:8001