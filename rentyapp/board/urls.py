#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.conf.urls import url
from board import views

urlpatterns = [
    url(r'^login/?$', views.login),
    url(r'^main/?$', views.dashboard),
    url(r'^orders/?$', views.order_list),
    url(r'^charges/?$', views.charges_list),
    url(r'^orders/charges/?$', views.order_charges_list),
    url(r'^users/?$', views.user_list),
    url(r'^property/?$', views.property_list),
    url(r'^resident/?$', views.resident_list),
    url(r'^contract/?$', views.contract_list),
    url(r'^logout/?$', views.logout),
    url(r'^service/types/?$', views.service_type_list),
    url(r'^payment/property/?$', views.payment_property_list),
    url(r'^users/edit/?$', views.edit_profile),
    url(r'^users/add/?$', views.add_profile),
    url(r'^users/delete/(?P<user_id>[\w\-]+)/?$', views.delete_user),
    url(r'^orders/edit/(?P<order_id>[\w\-]+)/?$', views.edit_order),#{10}
    url(r'^charges/edit/(?P<charge_id>[\w\-]+)/?$', views.edit_charge),#{10}
    url(r'^orders/charges/edit/(?P<charge_id>[\w\-]+)/?$', views.edit_order_charge),#{10}
    url(r'^users/edit/(?P<user_id>[\w\-]+)/?$', views.edit_profiles),
    url(r'^property/edit/(?P<property_id>[\w\-]+)/?$', views.edit_property),
    url(r'^service/types/edit/(?P<service_id>[\w\-]+)/?$', views.edit_service),
    url(r'^resident/edit/(?P<resident_id>[\w\-]+)/?$', views.edit_residents),
    url(r'^contract/edit/(?P<contract_id>[\w\-]+)/?$', views.edit_contract),
    url(r'^service/types/add/?$', views.add_service),
    url(r'^payment/download/xls/?$', views.excel_bank_export),
    # (?P<path>.*)$

]
