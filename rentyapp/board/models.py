# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models


class Categories(models.Model):
    name = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    available = models.NullBooleanField()

    class Meta:
        managed = False
        db_table = 'categories'

    def __str__(self):
        return '%s' % (self.name)


class Charges(models.Model):
    amount = models.IntegerField(blank=True, null=True)
    contract = models.ForeignKey('Contracts', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=False)
    status = models.NullBooleanField(default=False)
    current_value = models.FloatField(blank=True, null=True, default=0)
    readjustment = models.NullBooleanField(default=False)
    pay_date = models.DateField(auto_now_add=False)

    class Meta:
        managed = False
        db_table = 'charges'

    def __str__(self):
        return '%s' % (self.contract)


class Communes(models.Model):
    name = models.CharField(max_length=1, blank=True, null=True)
    region = models.ForeignKey('Regions', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'communes'

    def __str__(self):
        return self.name


class Contracts(models.Model):
    fare_type = models.CharField(max_length=100, blank=True, null=True)
    amount = models.IntegerField(blank=True, null=True)
    pay_day = models.IntegerField(blank=True, null=True)
    start_rent = models.DateField(blank=True, null=True)
    end_rent = models.DateField(blank=True, null=True)
    contract_period = models.CharField(max_length=100, blank=True, null=True)
    status = models.NullBooleanField(default=False)
    automatic_renewal = models.NullBooleanField()
    pdf = models.CharField(max_length=100, blank=True, null=True)
    ipc_readjustment = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=False)
    property = models.ForeignKey('Properties', models.DO_NOTHING, blank=True, null=True)
    resident = models.ForeignKey('Residents', models.DO_NOTHING, blank=True, null=True)
    clp_to_uf = models.FloatField(blank=True, null=True)
    readjustment = models.NullBooleanField(default=False)

    def __str__(self):
        return '%s' % (self.fare_type)

    class Meta:
        managed = False
        db_table = 'contracts'





class Fixes(models.Model):
    description = models.TextField(blank=True, null=True)
    amount = models.IntegerField(blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    status = models.NullBooleanField()
    deducted = models.NullBooleanField()
    contract = models.ForeignKey(Contracts, models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    title = models.CharField(max_length=1, blank=True, null=True)
    response = models.TextField(blank=True, null=True)
    charge = models.ForeignKey(Charges, models.DO_NOTHING, blank=True, null=True)
    kind = models.CharField(max_length=1, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'fixes'

    def __str__(self):
        return '%s' % (self.contract)


class OrderCharges(models.Model):
    charge = models.ForeignKey(Charges, models.DO_NOTHING, blank=True, null=True)
    order = models.ForeignKey('Orders', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField(auto_created=True)
    updated_at = models.DateTimeField(auto_now_add=False)
    amount = models.IntegerField(blank=True, null=True,default=0)

    class Meta:
        managed = False
        db_table = 'order_charges'

    def __str__(self):
        return '%s' % (self.order)


class Orders(models.Model):
    provider = models.CharField(max_length=100, blank=True, null=True)
    status = models.NullBooleanField(default=False)
    amount = models.IntegerField(blank=True, null=True, default=0)
    file = models.CharField(max_length=100, blank=True, null=True)
    comment = models.TextField(blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_created=False)
    resident_id = models.IntegerField(blank=True, null=True, default=0)
    contract_id = models.IntegerField(blank=True, null=True, default=0)
    code = models.IntegerField(blank=True, null=True)
    tbk_accounting_date = models.CharField(max_length=1, blank=True, null=True)
    tbk_buy_order = models.CharField(max_length=100, blank=True, null=True)
    tbk_cardnumber = models.CharField(max_length=100, blank=True, null=True)
    tbk_commerce_code = models.CharField(max_length=100, blank=True, null=True)
    tbk_authorization_code = models.CharField(max_length=100, blank=True, null=True)
    tbk_payment_type_code = models.CharField(max_length=100, blank=True, null=True)
    tbk_responde_code = models.CharField(max_length=100, blank=True, null=True)
    tbk_transaction_date = models.CharField(max_length=100, blank=True, null=True)
    tbk_vci = models.CharField(max_length=100, blank=True, null=True)
    tbk_amount = models.CharField(max_length=100, blank=True, null=True)
    token = models.CharField(max_length=100, blank=True, null=True)
    tbk_shares = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'orders'

    def __str__(self):
        return '%s' % (self.code)


class Owners(models.Model):
    name = models.CharField(max_length=1, blank=True, null=True)
    last_name = models.CharField(max_length=1, blank=True, null=True)
    rut = models.CharField(max_length=1, blank=True, null=True)
    agent = models.NullBooleanField()
    phone = models.CharField(max_length=1, blank=True, null=True)
    address = models.CharField(max_length=1, blank=True, null=True)
    code = models.CharField(max_length=1, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'owners'

    def __str__(self):
        return "{0} {1}".format(self.name, self.last_name)

class Properties(models.Model):

    category = models.ForeignKey(Categories, models.DO_NOTHING, default=0)
    region = models.ForeignKey('Regions', models.DO_NOTHING, default=0)
    commune = models.ForeignKey(Communes, models.DO_NOTHING, default=0)
    street = models.CharField(max_length=100,default=0)
    street_number = models.CharField(max_length=10,default=0)
    property_number = models.CharField(max_length=10,default=0)
    size = models.IntegerField(default=0)
    image = models.CharField(max_length=250, default='image.png')
    name_concierge = models.CharField(max_length=100)
    phone_concierge = models.CharField(max_length=100)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=False)
    owner = models.ForeignKey(Owners, models.DO_NOTHING, default=0)

    class Meta:
        managed = False
        db_table = 'properties'

    def __str__(self):
        return '%s - P/N: (%s)' % (self.owner, self.property_number)




class PaymentProperties(models.Model):
    property = models.ForeignKey('Properties', models.DO_NOTHING, blank=True, null=True)
    bank = models.CharField(max_length=100, blank=True, null=True)
    account_type = models.CharField(max_length=100, blank=True, null=True)
    account_number = models.CharField(max_length=100, blank=True, null=True)
    name_holder = models.CharField(max_length=100, blank=True, null=True)
    rut_holder = models.CharField(max_length=20, blank=True, null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=False)
    email = models.CharField(max_length=100, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'payment_properties'

    def __str__(self):
        return '%s' % (self.property)


class PreUsers(models.Model):
    name = models.CharField(max_length=1, blank=True, null=True)
    last_name = models.CharField(max_length=1, blank=True, null=True)
    email = models.CharField(max_length=1, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'pre_users'

    def __str__(self):
        return "{0} {1}".format(self.name, self.last_name)



class Regions(models.Model):
    number = models.IntegerField(blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()

    class Meta:
        managed = False
        db_table = 'regions'

    def __str__(self):
        return '%s' % (self.name)


class Residents(models.Model):

    name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    rut = models.CharField(max_length=20)
    email = models.CharField(max_length=100)
    phone = models.CharField(max_length=20)
    score = models.IntegerField(blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=False)
    user = models.ForeignKey('Users', models.DO_NOTHING)

    class Meta:
        managed = False
        db_table = 'residents'

    def __str__(self):
        return "{0} {1}".format(self.name, self.last_name)


class ServiceTypes(models.Model):
    name = models.CharField(max_length=100,null=False)
    state = models.NullBooleanField(default=False)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=False)

    class Meta:
        managed = False
        db_table = 'service_types'

    def __str__(self):
        return "{0}".format(self.name)


class Services(models.Model):
    service_type = models.ForeignKey(ServiceTypes, models.DO_NOTHING, blank=True, null=True)
    user = models.ForeignKey('Users', models.DO_NOTHING, blank=True, null=True)
    date = models.DateField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    description = models.TextField(blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'services'

    def __str__(self):
        return "{0}".format(self.service_type)


class Supports(models.Model):
    name = models.CharField(max_length=1, blank=True, null=True)
    last_name = models.CharField(max_length=1, blank=True, null=True)
    rut = models.CharField(max_length=1, blank=True, null=True)
    phone = models.CharField(max_length=1, blank=True, null=True)
    email = models.CharField(max_length=1, blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    resident = models.ForeignKey(Residents, models.DO_NOTHING, blank=True, null=True)

    class Meta:
        managed = False
        db_table = 'supports'

    def __unicode__(self):
        return "{0} {1}".format(self.name, self.last_name)


class Users(models.Model):
    email = models.CharField(unique=True, max_length=100)
    encrypted_password = models.CharField(max_length=250)
    reset_password_token = models.CharField(unique=True, max_length=1, blank=True, null=True)
    reset_password_sent_at = models.DateTimeField(blank=True, null=True)
    remember_created_at = models.DateTimeField(blank=True, null=True)
    sign_in_count = models.IntegerField()
    current_sign_in_at = models.DateTimeField(blank=True, null=True)
    last_sign_in_at = models.DateTimeField(blank=True, null=True)
    current_sign_in_ip = models.GenericIPAddressField(blank=True, null=True)
    last_sign_in_ip = models.GenericIPAddressField(blank=True, null=True)
    created_at = models.DateTimeField()
    updated_at = models.DateTimeField()
    uid = models.CharField(max_length=1, blank=True, null=True)
    provider = models.CharField(max_length=100, blank=True, null=True)
    name = models.CharField(max_length=100, blank=True, null=True)
    last_name = models.CharField(max_length=100, blank=True, null=True)
    admin = models.NullBooleanField(default=False)

    class Meta:
        managed = False
        db_table = 'users'

    def __str__(self):
        return "{0} {1}".format(self.name, self.last_name)
