# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import datetime
import hashlib
import json
import re

import io

from django.conf import settings
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Count
from django.shortcuts import render, render_to_response
from django.http import HttpResponseRedirect, Http404, HttpResponse
from django.template.context_processors import csrf
from django.views.decorators.csrf import csrf_exempt
from xlsxwriter.workbook import Workbook

from .forms import user_form, order_form, charge_form, order_charge_form, property_form, service_form, resident_form, \
    contract_form, search_form, login_form
from .models import Users, Orders, Charges, OrderCharges, Properties, Residents
from .models import Contracts, ServiceTypes, PaymentProperties

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO


# LIST

def dashboard(request):
    try:
        user_object = Users.objects.get(id='4')  # , admin=True
        request.session['admin_session'] = user_object.id

        properties_object = Properties.objects.values('region__name').annotate(Count('region')).order_by(
            'region__count')
        bank_object = PaymentProperties.objects.values('bank').annotate(Count('bank')).order_by('bank__count')

        year = datetime.date.today().year
        contracts = Contracts.objects.filter(created_at__year=year).count()
        charges = Charges.objects.filter(created_at__year=year).count()

        qs = (Charges.objects.all().extra(select={'month': "EXTRACT(month FROM created_at)",
                                                  'year': "EXTRACT(year FROM created_at)",
                                                  'month_str':"TO_CHAR(created_at,'Mon')"}).values('month', 'year', 'month_str').annotate(count_items=Count('id')))


        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/index.html',
                      {'user': user_object,
                       'menu': 'dashboard',
                       'region_stat': properties_object,
                       'bank_stat': bank_object,
                       'charges':charges,
                       'charges_graph': qs,
                       'contracts':contracts})

    except Users.DoesNotExist:
        Http404("No User matches the given query.")


def order_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                order_object = Orders.objects.all()
                paginator = Paginator(order_object, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/orders/order_list.html',
                              {'user': user_object,
                               'orders': po,
                               'menu': 'dashboard'})
            except Orders.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def charges_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                charges_object = Charges.objects.all()

                paginator = Paginator(charges_object, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/charges/charges_list.html',
                              {'user': user_object,
                               'charges': po,
                               'menu': 'dashboard'})
            except Charges.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def order_charges_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                order_charges_object = OrderCharges.objects.all()

                paginator = Paginator(order_charges_object, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/orders/order_charges_list.html',
                              {'user': user_object,
                               'charges': po,
                               'menu': 'dashboard'})
            except OrderCharges.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def user_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True

            try:
                query = request.GET.get('query')

                if query:
                    match = re.match('^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$', query)
                    if match:
                        user_object_list = Users.objects.filter(email__contains=query)
                    elif query.isalnum():
                        user_object_list = Users.objects.filter(name__contains=query)
                    else:
                        user_object_list = Users.objects.filter(last_name__contains=query)
                else:
                    user_object_list = Users.objects.all()

                paginator = Paginator(user_object_list, 100)
                page = request.GET.get('page')
                url = "&query={0}".format(query)

                try:
                    po = paginator.page(page)
                except PageNotAnInteger:
                    po = paginator.page(1)
                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/user_list.html',
                              {'user': user_object,
                               'users': po,
                               'url': url,
                               'menu': 'dashboard'})

            except Users.DoesNotExist:
                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/user_list.html',
                              {'user': user_object,
                               'users': [],
                               'query': True,
                               'menu': 'dashboard'})

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def property_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                property_object_list = Properties.objects.all()

                paginator = Paginator(property_object_list, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/property/property_list.html',
                              {'user': user_object,
                               'property_list': po,
                               'menu': 'dashboard'})
            except Users.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def resident_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                resident_object_list = Residents.objects.all()

                paginator = Paginator(resident_object_list, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/residents/resident_list.html',
                              {'user': user_object,
                               'resident_list': po,
                               'menu': 'dashboard'})
            except Users.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def contract_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                contract_object_list = Contracts.objects.all()

                paginator = Paginator(contract_object_list, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/contract/contract_list.html',
                              {'user': user_object,
                               'contract_list': po,
                               'menu': 'dashboard'})
            except Users.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


def service_type_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True
            try:
                service_types_list = ServiceTypes.objects.all()

                paginator = Paginator(service_types_list, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/st_list.html',
                              {'user': user_object,
                               'st_list': po,
                               'menu': 'dashboard'})
            except Users.DoesNotExist:
                return Http404("No Orders matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")


# payment_property_list
def payment_property_list(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:

        try:

            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)  # , admin=True

            try:
                pp_list = PaymentProperties.objects.all()

                paginator = Paginator(pp_list, 100)
                page = request.GET.get('page')
                # url = "&transaction_id={0}".format(transaction_id)

                try:
                    po = paginator.page(page)

                except PageNotAnInteger:
                    po = paginator.page(1)

                except EmptyPage:
                    po = paginator.page(paginator.num_pages)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/pp_list.html', {'user': user_object,
                                                                                                    'pp_list': po,
                                                                                                    'menu': 'dashboard'})
            except Users.DoesNotExist:
                return Http404("No Payment Properties matches the given query.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")

def excel_bank_export(request):

    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        try:
            session_key = request.session['admin_session']
            user_object = Users.objects.get(id=session_key)


            if user_object.admin:

                #workbook = xlsxwriter.Workbook('hello.xlsx')
                output = io.BytesIO()
                workbook = Workbook(output, {'in_memory': True})
                worksheet = workbook.add_worksheet()

                # Columns
                worksheet.write('A1', 'Cuenta Origen')
                worksheet.write('B1', 'Moneda Cuenta Origen')
                worksheet.write('C1', 'Cuenta Destino')
                worksheet.write('D1', 'Moneda Cuenta Destino')
                worksheet.write('E1', "Codigo Banco")
                worksheet.write('F1', 'Rut Beneficiario')
                worksheet.write('G1', 'Nombre Beneficiario')
                worksheet.write('H1', 'Monto Transferencia')
                worksheet.write('I1', 'Glosa Transferencia')
                worksheet.write('J1', 'Direccion Correo Beneficiario')
                worksheet.write('K1', 'Glosa Correo Beneficiario')
                worksheet.write('L1', 'Glosa Cartola Cliente')
                worksheet.write('M1', 'Glosa Cartola Beneficiario')

                row = 1
                #col = 0

                expenses = (
                    ['123456789011121', "CLP", "123456789011100", "CLP", "0037", "Alvaro Chavez", "1.000.000,00", "0", "0",
                     "ac@renty.cl", "0", "0", "0"],
                    ['123456789011121', "CLP", "123456789011100", "CLP", "0037", "Eddwin Paz", "1.000.000,00", "0", "0",
                     "ep@renty.cl", "0", "0", "0"],
                    ['123456789011121', "CLP", "123456789011100", "CLP", "0037", "Jhon Doe", "2.000.000,00", "0", "0",
                     "jd@renty.cl", "0", "0", "0"],
                )

                # Write Rows
                for item in (expenses):
                    worksheet.write(row, 0, item[0])
                    worksheet.write(row, 1, item[1])
                    worksheet.write(row, 2, item[2])
                    worksheet.write(row, 3, item[3])
                    worksheet.write(row, 4, item[4])
                    worksheet.write(row, 5, item[5])
                    worksheet.write(row, 6, item[6])
                    worksheet.write(row, 7, item[7])
                    worksheet.write(row, 8, item[8])
                    worksheet.write(row, 9, item[9])
                    worksheet.write(row, 10, item[10])
                    worksheet.write(row, 11, item[11])
                    worksheet.write(row, 12, item[12])
                    row += 1

                workbook.close()

                output.seek(0)

                response = HttpResponse(output.read(),
                                        content_type="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                response['Content-Disposition'] = "attachment; filename=santander_masivo.xlsx"

                return response

            else:
                return Http404("No User lacks of privileges.")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")



def add_profile(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                form = user_form(request.POST)

                if form.is_valid():

                    user_object_add = Users(
                        name=form.cleaned_data['name'],
                        last_name=form.cleaned_data['last_name'],
                        email=form.cleaned_data['email'],
                        provider=form.cleaned_data['provider'],
                        admin=form.cleaned_data['admin'],
                        sign_in_count=0,
                        created_at=datetime.datetime.now(),
                        updated_at=datetime.datetime.now()
                    )

                    user_object_add.save()

                    form = user_form()

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/add_profile.html',
                                  {'user': user_object,
                                   'form': form,
                                   'saved': True,
                                   'menu': 'dashboard'})
                else:

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/add_profile.html',
                                  {'user': user_object,
                                   'form': form,
                                   'saved': False,
                                   'menu': 'dashboard'})

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                form = user_form()

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/add_profile.html',
                              {'user': user_object,
                               'form': form,
                               'menu': 'dashboard'})

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")



def edit_profile(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                form = user_form(request.POST, instance=user_object)

                if form.is_valid():

                    user_object_edit = Users.objects.get(id=session_key)

                    user_object_edit.name = form.cleaned_data['name']
                    user_object_edit.last_name = form.cleaned_data['last_name']
                    user_object_edit.email = form.cleaned_data['email']
                    user_object_edit.provider = form.cleaned_data['provider']
                    user_object_edit.admin = form.cleaned_data['admin']


                    user_object_edit.save()

                    form = user_form(instance=user_object_edit)

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/edit_profile.html',
                                  {'user': user_object,
                                   'form': form,
                                   'saved': True,
                                   'menu': 'dashboard'})
                else:
                    form = user_form(instance=user_object)
                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/edit_profile.html',
                                  {'user': user_object,
                                   'form': form,
                                   'saved': False,
                                   'menu': 'dashboard'})

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                form = user_form(instance=user_object)

                return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/edit_profile.html',
                              {'user': user_object,
                               'form': form,
                               'menu': 'dashboard'})

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def logout(request):
    if 'admin_session' in request.session:
        try:
            del request.session['admin_session']
        except KeyError:
            pass
        return HttpResponseRedirect('/board/login/')
    else:
        return HttpResponseRedirect('/board/login/')


# EDIT

def edit_order(request, order_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':
            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    order_object = Orders.objects.get(id=order_id)
                    form = order_form(request.POST, instance=order_object)

                    if form.is_valid():

                        order_object = Orders.objects.get(id=order_id)

                        order_object.provider = form.cleaned_data['provider']
                        order_object.amount = form.cleaned_data['amount']
                        order_object.file = form.cleaned_data['file']
                        order_object.code = form.cleaned_data['code']
                        order_object.token = form.cleaned_data['token']

                        order_object.save()

                        form = order_form(instance=order_object)

                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/orders/order_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'saved': True,
                                       'order_object': order_object,
                                       'menu': 'dashboard'})
                    else:
                        form = order_form(instance=order_object)
                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/orders/order_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'saved': False,
                                       'order_object': order_object,
                                       'menu': 'dashboard'})

                except Orders.DoesNotExist:
                    return Http404("No Order matches the given query.")



            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    order_object = Orders.objects.get(id=order_id)
                    form = order_form(instance=order_object)

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/orders/order_edit.html',
                                  {'user': user_object,
                                   'form': form,
                                   'order_object': order_object,
                                   'menu': 'dashboard'})

                except Orders.DoesNotExist:
                    return Http404("No Order matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_charge(request, charge_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':
            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    charge_object = Charges.objects.get(id=charge_id)
                    form = charge_form(request.POST, instance=charge_object)

                    if form.is_valid():

                        try:
                            charge_object = Charges.objects.get(id=charge_id)

                            charge_object.contract = form.cleaned_data['contract']
                            charge_object.amount = form.cleaned_data['amount']
                            charge_object.status = form.cleaned_data['status']
                            charge_object.current_value = form.cleaned_data['current_value']
                            charge_object.readjustment = form.cleaned_data['readjustment']
                            charge_object.pay_date = form.cleaned_data['pay_date']

                            charge_object.save()

                            form = charge_form(instance=charge_object)

                            return render(request,
                                          settings.TEMPLATE_THEME_VERSION + 'dashboard/charges/charges_edit.html',
                                          {'user': user_object,
                                           'form': form,
                                           'saved': True,
                                           'charge_object': charge_object,
                                           'menu': 'dashboard'})

                        except Charges.DoesNotExist:
                            return Http404("No Charges matches the given query.")

                    else:
                        form = charge_form(instance=charge_object)
                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/charges/charges_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'saved': False,
                                       'charge_object': charge_object,
                                       'menu': 'dashboard'})

                except Charges.DoesNotExist:
                    return Http404("No Charges matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    charge_object = Charges.objects.get(id=charge_id)
                    form = charge_form(instance=charge_object)

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/charges/charges_edit.html',
                                  {'user': user_object,
                                   'form': form,
                                   'charge_object': charge_object,
                                   'menu': 'dashboard'})

                except Charges.DoesNotExist:
                    return Http404("No Charges matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_order_charge(request, charge_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':
            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    charge_object = OrderCharges.objects.get(id=charge_id)
                    form = order_charge_form(request.POST, instance=charge_object)

                    if form.is_valid():

                        try:
                            charge_object = OrderCharges.objects.get(id=charge_id)

                            # charge_object.contract = form.cleaned_data['contract']
                            charge_object.amount = form.cleaned_data['amount']
                            # charge_object.status = form.cleaned_data['status']
                            # charge_object.current_value = form.cleaned_data['current_value']
                            # charge_object.readjustment = form.cleaned_data['readjustment']
                            # charge_object.pay_date = form.cleaned_data['pay_date']

                            charge_object.save()

                            form = order_charge_form(instance=charge_object)

                            return render(request,
                                          settings.TEMPLATE_THEME_VERSION + 'dashboard/order_charges/order_charges_edit.html',
                                          {'user': user_object,
                                           'form': form,
                                           'saved': True,
                                           'charge_object': charge_object,
                                           'menu': 'dashboard'})

                        except OrderCharges.DoesNotExist:
                            return Http404("No Charges matches the given query.")

                    else:
                        form = order_charge_form(instance=charge_object)
                        return render(request,
                                      settings.TEMPLATE_THEME_VERSION + 'dashboard/order_charges/order_charges_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'saved': False,
                                       'charge_object': charge_object,
                                       'menu': 'dashboard'})

                except OrderCharges.DoesNotExist:
                    return Http404("No Charges matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    charge_object = OrderCharges.objects.get(id=charge_id)
                    form = order_charge_form(instance=charge_object)

                    return render(request,
                                  settings.TEMPLATE_THEME_VERSION + 'dashboard/order_charges/order_charges_edit.html',
                                  {'user': user_object,
                                   'form': form,
                                   'charge_object': charge_object,
                                   'menu': 'dashboard'})

                except OrderCharges.DoesNotExist:
                    return Http404("No Charges matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_profiles(request, user_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    user_object_edit = Users.objects.get(id=user_id)
                    form = user_form(request.POST, instance=user_object_edit)

                    if form.is_valid():

                        user_object_update = Users.objects.get(id=user_id)

                        user_object_update.name = form.cleaned_data['name']
                        user_object_update.last_name = form.cleaned_data['last_name']
                        user_object_update.email = form.cleaned_data['email']
                        user_object_update.provider = form.cleaned_data['provider']
                        user_object_update.admin = form.cleaned_data['admin']


                        user_object_update.save()

                        form = user_form(instance=user_object_update)

                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/edit_profiles.html',
                                      {'user': user_object,
                                       'form': form,
                                       'user_edit': user_object_edit,
                                       'saved': True,
                                       'menu': 'dashboard'})
                    else:
                        form = user_form(instance=user_object_edit)
                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/edit_profiles.html',
                                      {'user': user_object,
                                       'form': form,
                                       'user_edit': user_object_edit,
                                       'saved': False,
                                       'menu': 'dashboard'})
                except Users.DoesNotExist:
                    return Http404("No User matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    user_object_edit = Users.objects.get(id=user_id)
                    form = user_form(instance=user_object_edit)

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/users/edit_profiles.html',
                                  {'user': user_object,
                                   'user_edit': user_object_edit,
                                   'form': form,
                                   'menu': 'dashboard'})

                except Users.DoesNotExist:
                    return Http404("No User matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_property(request, property_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    property_object_edit = Properties.objects.get(id=property_id)
                    form = property_form(request.POST, instance=property_object_edit)

                    if form.is_valid():

                        property_object_update = Properties.objects.get(id=property_id)

                        property_object_update.owner = form.cleaned_data['owner']
                        property_object_update.category = form.cleaned_data['category']
                        property_object_update.region = form.cleaned_data['region']
                        property_object_update.commune = form.cleaned_data['commune']
                        property_object_update.street = form.cleaned_data['street']
                        property_object_update.street_number = form.cleaned_data['street_number']
                        property_object_update.property_number = form.cleaned_data['property_number']
                        property_object_update.size = form.cleaned_data['size']
                        property_object_update.name_concierge = form.cleaned_data['name_concierge']
                        property_object_update.phone_concierge = form.cleaned_data['phone_concierge']

                        property_object_update.save()

                        # form = property_form(instance=property_object_update)

                        return render(request,
                                      settings.TEMPLATE_THEME_VERSION + 'dashboard/property/property_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'property_edit': property_object_edit,
                                       'saved': True,

                                       'menu': 'dashboard'})
                    else:
                        form = property_form(instance=property_object_edit)
                        return render(request,
                                      settings.TEMPLATE_THEME_VERSION + 'dashboard/property/property_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'property_edit': property_object_edit,
                                       'saved': False,

                                       'menu': 'dashboard'})
                except Properties.DoesNotExist:
                    return Http404("No User matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    property_object_edit = Properties.objects.get(id=property_id)
                    form = property_form(instance=property_object_edit)

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/property/property_edit.html',
                                  {'user': user_object,
                                   'property_edit': property_object_edit,
                                   'form': form,
                                   'menu': 'dashboard'})

                except Properties.DoesNotExist:
                    return Http404("No User matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_service(request, service_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    service_object_edit = ServiceTypes.objects.get(id=service_id)
                    form = service_form(request.POST, instance=service_object_edit)

                    if form.is_valid():

                        service_object_update = ServiceTypes.objects.get(id=service_id)

                        service_object_update.name = form.cleaned_data['name']
                        service_object_update.state = form.cleaned_data['state']

                        service_object_update.save()

                        # form = property_form(instance=property_object_update)

                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/service_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'service_edit': service_object_edit,
                                       'saved': True,

                                       'menu': 'dashboard'})
                    else:
                        form = service_form(instance=service_object_edit)
                        return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/service_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'service_edit': service_object_edit,
                                       'saved': False,

                                       'menu': 'dashboard'})
                except ServiceTypes.DoesNotExist:
                    return Http404("No User matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    service_object_edit = ServiceTypes.objects.get(id=service_id)
                    form = service_form(instance=service_object_edit)

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/service_edit.html',
                                  {'user': user_object,
                                   'service_edit': service_object_edit,
                                   'form': form,
                                   'menu': 'dashboard'})

                except ServiceTypes.DoesNotExist:
                    return Http404("No User matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_contract(request, contract_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':
            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    contract_object = Contracts.objects.get(id=contract_id)
                    form = contract_form(request.POST, instance=contract_object)

                    if form.is_valid():

                        try:

                            contract_object.pay_day = form.cleaned_data['pay_day']
                            contract_object.start_rent = form.cleaned_data['start_rent']
                            contract_object.end_rent = form.cleaned_data['end_rent']
                            contract_object.contract_period = form.cleaned_data['contract_period']
                            contract_object.property = form.cleaned_data['property']
                            contract_object.resident = form.cleaned_data['resident']
                            contract_object.updated_at = datetime.datetime.now()


                            contract_object.save()

                            form = contract_form(instance=contract_object)

                            return render(request,
                                          settings.TEMPLATE_THEME_VERSION + 'dashboard/contract/contract_edit.html',
                                          {'user': user_object,
                                           'form': form,
                                           'saved': True,
                                           'contract_object': contract_object,
                                           'menu': 'dashboard'})

                        except Contracts.DoesNotExist:
                            return Http404("No Charges matches the given query.")

                    else:
                        form = contract_form(instance=contract_object)
                        return render(request,
                                      settings.TEMPLATE_THEME_VERSION + 'dashboard/contract/contract_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'saved': False,
                                       'contract_object': contract_object,
                                       'menu': 'dashboard'})

                except Contracts.DoesNotExist:
                    return Http404("No Charges matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    contract_object = Contracts.objects.get(id=contract_id)
                    form = contract_form(instance=contract_object)

                    return render(request,
                                  settings.TEMPLATE_THEME_VERSION + 'dashboard/contract/contract_edit.html',
                                  {'user': user_object,
                                   'form': form,
                                   'contract_object': contract_object,
                                   'menu': 'dashboard'})

                except Contracts.DoesNotExist:
                    return Http404("No Charges matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")


def edit_residents(request, resident_id):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':
            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    resident_object = Residents.objects.get(id=resident_id)
                    form = resident_form(request.POST, instance=resident_object)

                    if form.is_valid():

                        try:
                            resident_object.name = form.cleaned_data['name']
                            resident_object.last_name = form.cleaned_data['last_name']
                            resident_object.rut = form.cleaned_data['rut']
                            resident_object.email = form.cleaned_data['email']
                            resident_object.phone = form.cleaned_data['phone']
                            resident_object.score = form.cleaned_data['score']
                            resident_object.updated_at = datetime.datetime.now()
                            resident_object.user = form.cleaned_data['user']

                            resident_object.save()

                            form = resident_form(instance=resident_object)

                            return render(request,
                                          settings.TEMPLATE_THEME_VERSION + 'dashboard/residents/residents_edit.html',
                                          {'user': user_object,
                                           'form': form,
                                           'saved': True,
                                           'resident_object': resident_object,
                                           'menu': 'dashboard'})

                        except Residents.DoesNotExist:
                            return Http404("No Charges matches the given query.")

                    else:
                        form = resident_form(instance=resident_object)
                        return render(request,
                                      settings.TEMPLATE_THEME_VERSION + 'dashboard/residents/residents_edit.html',
                                      {'user': user_object,
                                       'form': form,
                                       'saved': False,
                                       'resident_object': resident_object,
                                       'menu': 'dashboard'})

                except Residents.DoesNotExist:
                    return Http404("No Charges matches the given query.")

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                try:
                    resident_object = Residents.objects.get(id=resident_id)
                    form = resident_form(instance=resident_object)

                    return render(request,
                                  settings.TEMPLATE_THEME_VERSION + 'dashboard/residents/residents_edit.html',
                                  {'user': user_object,
                                   'form': form,
                                   'resident_object': resident_object,
                                   'menu': 'dashboard'})

                except Residents.DoesNotExist:
                    return Http404("No Charges matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

@csrf_exempt
def delete_user(request, user_id):

    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']
        try:
            admin_object = Users.objects.get(id=session_key)

            if admin_object.admin:

                if request.method == 'GET':
                    try:
                        user_object = Users.objects.get(id=user_id)
                        user_object.delete()

                        data = {'detalle': "User deleted", 'response': True}
                        return HttpResponse(json.dumps(data), content_type='application/json', status=200)

                    except Users.DoesNotExist:
                        return Http404("No User matches the given query.")
            else:
                return Http404("You don't have privileges to take on this action")

        except Users.DoesNotExist:
            return Http404("No User matches the given query.")
# ADD

def add_service(request):
    if 'admin_session' not in request.session:
        return HttpResponseRedirect('/board/login/')
    else:
        session_key = request.session['admin_session']

        if request.method == 'POST':

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True

                form = service_form(request.POST)

                if form.is_valid():

                    service_object = ServiceTypes(name=form.cleaned_data['name'],
                                                  state=form.cleaned_data['state'],
                                                  updated_at=datetime.datetime.now())
                    service_object.save()

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/service_add.html',
                                  {'user': user_object,
                                   'form': form,
                                   'saved': True,
                                   'menu': 'dashboard'})
                else:
                    form = service_form(request.POST)
                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/service_add.html',
                                  {'user': user_object,
                                   'form': form,
                                   'saved': False,
                                   'menu': 'dashboard'})

            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

        else:

            try:
                user_object = Users.objects.get(id=session_key)  # , admin=True
                try:
                    form = service_form()

                    return render(request, settings.TEMPLATE_THEME_VERSION + 'dashboard/service/service_add.html',
                                  {'user': user_object,
                                   'form': form,
                                   'menu': 'dashboard'})

                except ServiceTypes.DoesNotExist:
                    return Http404("No User matches the given query.")


            except Users.DoesNotExist:
                return Http404("No User matches the given query.")

# LOGIN
def login(request):
    if request.method == "POST":

        form = login_form(request.POST)

        if form.is_valid():

            email = form.cleaned_data['email']
            password = hashlib.sha1(form.cleaned_data['password'].encode('utf8')).hexdigest()
            email = email.encode('utf8')

            try:
                accounts = Users.objects.get(email=email)

                if accounts.encrypted_password == password:

                    request.session['admin_session'] = accounts.id
                    accounts.last_seen_date = datetime.datetime.now()

                    return HttpResponseRedirect('/board/main/')

                else:

                    form = login_form()
                    args = {}

                    args.update(csrf(request))
                    args['message'] = True
                    args['form'] = form

                    return render_to_response(settings.TEMPLATE_THEME_VERSION + 'dashboard/users/login.html', args)

            except Users.DoesNotExist:

                form = login_form()
                args = {}

                args.update(csrf(request))
                args['message'] = True
                args['form'] = form

                return render_to_response(settings.TEMPLATE_THEME_VERSION + 'dashboard/users/login.html', args)
    else:

        if 'admin_session' in request.session:
            return HttpResponseRedirect('/board/main/')
        else:
            form = login_form()

    args = {}
    args.update(csrf(request))
    args['form'] = form

    return render_to_response(settings.TEMPLATE_THEME_VERSION + 'dashboard/users/login.html', args)
