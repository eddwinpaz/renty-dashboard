from django import forms
from django.forms import ModelForm

from .models import Users, Orders, Charges, OrderCharges, Properties, ServiceTypes, Residents, Contracts


class search_form(forms.Form):
    search_query = forms.CharField(max_length=100, required=True)


class user_form(ModelForm):
    admin = forms.BooleanField(required=False)

    class Meta:
        model = Users

        fields = ['name', 'last_name', 'email', 'provider', 'admin']
        # error_messages = {'field': {'required':''}}

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'provider': forms.TextInput(attrs={'class': 'form-control'}),

        }




class order_form(ModelForm):
    # status = forms.BooleanField(required=False)

    class Meta:
        model = Orders

        fields = ['provider', 'amount', 'file', 'comment', 'code', 'token']
        #error_messages = {'provider': {'required':'Select provider'}}

        widgets = {
            'provider': forms.TextInput(attrs={'class': 'form-control'}),
            'amount': forms.TextInput(attrs={'class': 'form-control'}),
            'file': forms.TextInput(attrs={'class': 'form-control'}),
            'comment': forms.TextInput(attrs={'class': 'form-control'}),
            'code': forms.TextInput(attrs={'class': 'form-control'}),
            'token': forms.TextInput(attrs={'class': 'form-control'}),

        }

class charge_form(ModelForm):

    #status = forms.BooleanField(required=False)

    class Meta:
        model = Charges

        fields = ['contract', 'amount', 'status', 'current_value', 'readjustment', 'pay_date']
        # error_messages = {'field': {'required':''}}

        widgets = {
            'contract': forms.Select(attrs={'class': 'form-control'}),
            'amount': forms.NumberInput(attrs={'class': 'form-control'}),
            'current_value': forms.NumberInput(attrs={'class': 'form-control'}),
            'readjustment': forms.TextInput(attrs={'class': 'form-control'}),
            'pay_date': forms.TextInput(attrs={'class': 'form-control'}),


        }

class order_charge_form(ModelForm):

    class Meta:
        model = OrderCharges

        fields = ['charge', 'order', 'amount']
        # error_messages = {'field': {'required':''}}

        widgets = {
            'charge': forms.Select(attrs={'class': 'form-control'}),
            'amount': forms.TextInput(attrs={'class': 'form-control'}),
            'order': forms.Select(attrs={'class': 'form-control'}),

        }

class property_form(ModelForm):

    class Meta:

        model = Properties

        fields = ['category','owner','region','commune',
                  'street','street_number','property_number',
                  'size','name_concierge','phone_concierge']

        #error_messages = {'street': {'max_length':'Street name is longer than 100 characters'}}

        widgets = {
            'owner': forms.Select(attrs={'class': 'form-control'}),
            'category': forms.Select(attrs={'class': 'form-control'}),
            'region': forms.Select(attrs={'class': 'form-control'}),
            'commune': forms.Select(attrs={'class': 'form-control'}),
            'street': forms.TextInput(attrs={'class': 'form-control'}),
            'street_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'property_number': forms.NumberInput(attrs={'class': 'form-control'}),
            'size': forms.NumberInput(attrs={'class': 'form-control'}),
            'name_concierge': forms.TextInput(attrs={'class': 'form-control'}),
            'phone_concierge': forms.TextInput(attrs={'class': 'form-control'}),

        }


class service_form(ModelForm):

    #state = forms.BooleanField(required=False)
    #state = forms.TypedChoiceField(coerce=lambda x: x == 'True', choices=((False, 'No'), (True, 'Yes')))
    #state = forms.BooleanField(widget=forms.CheckboxInput, default=False)

    class Meta:

        model = ServiceTypes

        fields = ['name','state',]

        #error_messages = {'street': {'max_length':'Street name is longer than 100 characters'}}

        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control'}),
        }

class resident_form(ModelForm):

    #status = forms.BooleanField(required=False)

    class Meta:
        model = Residents

        fields = ['name', 'last_name', 'rut', 'email', 'phone', 'score','user',]
        # error_messages = {'field': {'required':''}}

        widgets = {
            'user': forms.Select(attrs={'class': 'form-control'}),
            'score': forms.NumberInput(attrs={'class': 'form-control'}),
            'phone': forms.NumberInput(attrs={'class': 'form-control'}),
            'rut': forms.NumberInput(attrs={'class': 'form-control'}),
            'name': forms.TextInput(attrs={'class': 'form-control'}),
            'last_name': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),


        }

class contract_form(ModelForm):

    #status = forms.BooleanField(required=False)

    class Meta:
        model = Contracts

        fields = ['pay_day', 'start_rent', 'end_rent', 'contract_period', 'property', 'resident']
        # error_messages = {'field': {'required':''}}

        widgets = {
            'resident': forms.Select(attrs={'class': 'form-control'}),
            'contract_period': forms.TextInput(attrs={'class': 'form-control'}),
            'start_rent': forms.TextInput(attrs={'class': 'form-control'}),
            'end_rent': forms.TextInput(attrs={'class': 'form-control'}),
            'pay_day': forms.NumberInput(attrs={'class': 'form-control'}),
            'property': forms.Select(attrs={'class': 'form-control'}),


        }

class login_form(forms.Form):

    email = forms.EmailField(
        max_length=100,
        label="Email",
        required=True,
        error_messages={
            'required': 'Por favor ingrese un email',
            'invalid': 'Por favor ingrese un email valido'}
    )

    password = forms.CharField(
        max_length=15,
        label=u"Contraseña",
        required=True,
        error_messages={
            'required': 'Por favor ingrese su contraseña'}
    )

    def clean_email(self):
        return self.cleaned_data['email'].lower()