# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from .models import *


class UsersAdmin(admin.ModelAdmin):
    list_display = ('name', 'last_name', 'sign_in_count',)
    search_fields = ('name', 'last_name', 'email', )

class PaymentPropertiesAdmin(admin.ModelAdmin):
    list_display = ('property', 'bank', 'name_holder', 'email',)
    search_fields = ('property', 'bank', 'name_holder', )

class ContractsAdmin(admin.ModelAdmin):
    list_display = ('start_rent', 'end_rent', 'status','pay_day')

# Register your models here.
admin.site.register(Categories)
admin.site.register(Charges)
admin.site.register(Communes)
admin.site.register(Contracts)
admin.site.register(Fixes)
admin.site.register(OrderCharges)
admin.site.register(Orders)
admin.site.register(PaymentProperties,PaymentPropertiesAdmin)
admin.site.register(PreUsers)
admin.site.register(Properties)
admin.site.register(Regions)
admin.site.register(Residents)
admin.site.register(ServiceTypes)
admin.site.register(Services)
admin.site.register(Supports)
admin.site.register(Users, UsersAdmin)